# Jitsi REST App
Jitsi REST App (JRA) is a tiny server which dumps a bot into a Jitsi conference room and exposes a couple of endpoints to grab participant details.

## Installation
Use [yarn](https://yarnpkg.com/getting-started/install) or [npm](https://www.npmjs.com/get-npm) to install JRA. You will also need [node](https://nodejs.org/en/download/) in order to run it. If you're running the app on Linux, you should be able to install these using the package manager.

Once one of these has been installed, navigate to the root directory of the repository and run:

```bash
yarn install
```

or:

```bash
npm install
```

The project dependencies should now be installed.

## Running
Once dependencies are installed, update the `config.json` file with details for the Jitsi conference room you wish the bot to connect to. The following options are available for configuration:
- `port`: the port on which the Jitsi REST App should be available
- `allowCustomBaseURLs`: a boolean indicating whether users can specify the base URLs of room resources they wish to connect to
- `baseURL`: the default base URL for room resources
- `allowCustomAvatarURLs`: a boolean indicating whether users can specify the avatar URLs of the bots they are connecting to rooms with
- `avatarURL`: the default avatar URL for bots

Then simply run:

```bash
node ./index.js
```

This will boot up the server on whatever port has been specified in the `config.json` file.

## Usage
Once the application is running, a room can be connected to by executing a POST request to the route `/rooms/` with the details of the room resource you wish to connect to. The following fields can be included:
- `baseURL`: the base URL for the Jitsi instance (if `allowCustomBaseURLs` has been enabled in the application config)
- `roomName`: the name of the room
- `displayName`: what display name the bot should have
- `avatarURL`: the URL for the bot's avatar (if `allowCustomAvatarURLs` has been enabled in the application config)

Once a room has been connected to, it will be available at the route `/rooms/:id`, where `:id` is the room's name. The following routes will be available on the room, and will respond to GET requests:
- `/rooms/:id/leave`: commands the bot to leave the room with name `:id`
- `/rooms/:id/numberofparticipants`: returns the number of participants in the room with name `:id`
- `/rooms/:id/participantsinfo`: returns information about the participants in room `:id`

Calling any of these routes will return a JSON object of the form `{ "data": data }`, where "data" is the response from the related function in the [Jitsi API](https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe).

## License
[MIT](https://choosealicense.com/licenses/mit/)